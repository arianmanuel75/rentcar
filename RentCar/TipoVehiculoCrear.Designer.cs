﻿namespace RentCar
{
    partial class TipoVehicloCrear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TipoVehicloCrear));
            this.dgvTipoVehiculo = new System.Windows.Forms.DataGridView();
            this.lblTipoVehiculoTitulo = new System.Windows.Forms.Label();
            this.btnTipoVehiculoGuardar = new System.Windows.Forms.Button();
            this.btnTipoVehiculoEditar = new System.Windows.Forms.Button();
            this.btnTipoVehiculoEliminar = new System.Windows.Forms.Button();
            this.txtTipoVehiculoDescripcion = new System.Windows.Forms.TextBox();
            this.lblTipoVehiculoDescripcion = new System.Windows.Forms.Label();
            this.btnTipoVehiculoCancelar = new System.Windows.Forms.Button();
            this.lblTipoVehiculoId = new System.Windows.Forms.Label();
            this.lblTipoVehiculoBuscar = new System.Windows.Forms.Label();
            this.txtTipoVehiculoBuscar = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTipoVehiculo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvTipoVehiculo
            // 
            this.dgvTipoVehiculo.AllowUserToAddRows = false;
            this.dgvTipoVehiculo.AllowUserToDeleteRows = false;
            this.dgvTipoVehiculo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTipoVehiculo.Location = new System.Drawing.Point(12, 113);
            this.dgvTipoVehiculo.Name = "dgvTipoVehiculo";
            this.dgvTipoVehiculo.ReadOnly = true;
            this.dgvTipoVehiculo.Size = new System.Drawing.Size(301, 203);
            this.dgvTipoVehiculo.TabIndex = 0;
            // 
            // lblTipoVehiculoTitulo
            // 
            this.lblTipoVehiculoTitulo.AutoSize = true;
            this.lblTipoVehiculoTitulo.BackColor = System.Drawing.SystemColors.Control;
            this.lblTipoVehiculoTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoVehiculoTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTipoVehiculoTitulo.Name = "lblTipoVehiculoTitulo";
            this.lblTipoVehiculoTitulo.Size = new System.Drawing.Size(404, 37);
            this.lblTipoVehiculoTitulo.TabIndex = 17;
            this.lblTipoVehiculoTitulo.Text = "Agregar Tipo de Vehiculo";
            // 
            // btnTipoVehiculoGuardar
            // 
            this.btnTipoVehiculoGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoVehiculoGuardar.Location = new System.Drawing.Point(422, 178);
            this.btnTipoVehiculoGuardar.Name = "btnTipoVehiculoGuardar";
            this.btnTipoVehiculoGuardar.Size = new System.Drawing.Size(90, 30);
            this.btnTipoVehiculoGuardar.TabIndex = 27;
            this.btnTipoVehiculoGuardar.Text = "Guardar";
            this.btnTipoVehiculoGuardar.UseVisualStyleBackColor = true;
            this.btnTipoVehiculoGuardar.Click += new System.EventHandler(this.btnTipoVehiculoGuardar_Click);
            // 
            // btnTipoVehiculoEditar
            // 
            this.btnTipoVehiculoEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoVehiculoEditar.Location = new System.Drawing.Point(422, 214);
            this.btnTipoVehiculoEditar.Name = "btnTipoVehiculoEditar";
            this.btnTipoVehiculoEditar.Size = new System.Drawing.Size(90, 30);
            this.btnTipoVehiculoEditar.TabIndex = 28;
            this.btnTipoVehiculoEditar.Text = "Editar";
            this.btnTipoVehiculoEditar.UseVisualStyleBackColor = true;
            this.btnTipoVehiculoEditar.Click += new System.EventHandler(this.btnTipoVehiculoEditar_Click);
            // 
            // btnTipoVehiculoEliminar
            // 
            this.btnTipoVehiculoEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoVehiculoEliminar.Location = new System.Drawing.Point(422, 250);
            this.btnTipoVehiculoEliminar.Name = "btnTipoVehiculoEliminar";
            this.btnTipoVehiculoEliminar.Size = new System.Drawing.Size(90, 30);
            this.btnTipoVehiculoEliminar.TabIndex = 29;
            this.btnTipoVehiculoEliminar.Text = "Eliminar";
            this.btnTipoVehiculoEliminar.UseVisualStyleBackColor = true;
            this.btnTipoVehiculoEliminar.Click += new System.EventHandler(this.btnTipoVehiculoEliminar_Click);
            // 
            // txtTipoVehiculoDescripcion
            // 
            this.txtTipoVehiculoDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTipoVehiculoDescripcion.Location = new System.Drawing.Point(343, 128);
            this.txtTipoVehiculoDescripcion.Name = "txtTipoVehiculoDescripcion";
            this.txtTipoVehiculoDescripcion.Size = new System.Drawing.Size(240, 26);
            this.txtTipoVehiculoDescripcion.TabIndex = 30;
            // 
            // lblTipoVehiculoDescripcion
            // 
            this.lblTipoVehiculoDescripcion.AutoSize = true;
            this.lblTipoVehiculoDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoVehiculoDescripcion.Location = new System.Drawing.Point(339, 101);
            this.lblTipoVehiculoDescripcion.Name = "lblTipoVehiculoDescripcion";
            this.lblTipoVehiculoDescripcion.Size = new System.Drawing.Size(115, 24);
            this.lblTipoVehiculoDescripcion.TabIndex = 31;
            this.lblTipoVehiculoDescripcion.Text = "Descripcion:";
            // 
            // btnTipoVehiculoCancelar
            // 
            this.btnTipoVehiculoCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoVehiculoCancelar.Location = new System.Drawing.Point(422, 286);
            this.btnTipoVehiculoCancelar.Name = "btnTipoVehiculoCancelar";
            this.btnTipoVehiculoCancelar.Size = new System.Drawing.Size(90, 30);
            this.btnTipoVehiculoCancelar.TabIndex = 32;
            this.btnTipoVehiculoCancelar.Text = "Cancelar";
            this.btnTipoVehiculoCancelar.UseVisualStyleBackColor = true;
            this.btnTipoVehiculoCancelar.Click += new System.EventHandler(this.btnTipoVehiculoCancelar_Click);
            // 
            // lblTipoVehiculoId
            // 
            this.lblTipoVehiculoId.AutoSize = true;
            this.lblTipoVehiculoId.BackColor = System.Drawing.SystemColors.Control;
            this.lblTipoVehiculoId.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoVehiculoId.Location = new System.Drawing.Point(396, 50);
            this.lblTipoVehiculoId.Name = "lblTipoVehiculoId";
            this.lblTipoVehiculoId.Size = new System.Drawing.Size(199, 29);
            this.lblTipoVehiculoId.TabIndex = 49;
            this.lblTipoVehiculoId.Text = "Tip. Vehiculo ID";
            // 
            // lblTipoVehiculoBuscar
            // 
            this.lblTipoVehiculoBuscar.AutoSize = true;
            this.lblTipoVehiculoBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoVehiculoBuscar.Location = new System.Drawing.Point(15, 80);
            this.lblTipoVehiculoBuscar.Name = "lblTipoVehiculoBuscar";
            this.lblTipoVehiculoBuscar.Size = new System.Drawing.Size(63, 20);
            this.lblTipoVehiculoBuscar.TabIndex = 51;
            this.lblTipoVehiculoBuscar.Text = "Buscar:";
            // 
            // txtTipoVehiculoBuscar
            // 
            this.txtTipoVehiculoBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTipoVehiculoBuscar.Location = new System.Drawing.Point(84, 77);
            this.txtTipoVehiculoBuscar.Name = "txtTipoVehiculoBuscar";
            this.txtTipoVehiculoBuscar.Size = new System.Drawing.Size(157, 26);
            this.txtTipoVehiculoBuscar.TabIndex = 50;
            this.txtTipoVehiculoBuscar.TextChanged += new System.EventHandler(this.txtTipoVehiculoBuscar_TextChanged);
            // 
            // TipoVehicloCrear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 328);
            this.Controls.Add(this.lblTipoVehiculoBuscar);
            this.Controls.Add(this.txtTipoVehiculoBuscar);
            this.Controls.Add(this.lblTipoVehiculoId);
            this.Controls.Add(this.btnTipoVehiculoCancelar);
            this.Controls.Add(this.lblTipoVehiculoDescripcion);
            this.Controls.Add(this.txtTipoVehiculoDescripcion);
            this.Controls.Add(this.btnTipoVehiculoEliminar);
            this.Controls.Add(this.btnTipoVehiculoEditar);
            this.Controls.Add(this.btnTipoVehiculoGuardar);
            this.Controls.Add(this.lblTipoVehiculoTitulo);
            this.Controls.Add(this.dgvTipoVehiculo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TipoVehicloCrear";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Crear Tipo de Vehiclo";
            this.Load += new System.EventHandler(this.TipoVehicloCrear_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTipoVehiculo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvTipoVehiculo;
        private System.Windows.Forms.Label lblTipoVehiculoTitulo;
        private System.Windows.Forms.Button btnTipoVehiculoGuardar;
        private System.Windows.Forms.Button btnTipoVehiculoEditar;
        private System.Windows.Forms.Button btnTipoVehiculoEliminar;
        private System.Windows.Forms.TextBox txtTipoVehiculoDescripcion;
        private System.Windows.Forms.Label lblTipoVehiculoDescripcion;
        private System.Windows.Forms.Button btnTipoVehiculoCancelar;
        private System.Windows.Forms.Label lblTipoVehiculoId;
        private System.Windows.Forms.Label lblTipoVehiculoBuscar;
        private System.Windows.Forms.TextBox txtTipoVehiculoBuscar;
    }
}