﻿namespace RentCar
{
    partial class RentaDevolverLista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RentaDevolverLista));
            this.lblRentaBuscar = new System.Windows.Forms.Label();
            this.txtRentaBuscar = new System.Windows.Forms.TextBox();
            this.btnDevolverLCancelar = new System.Windows.Forms.Button();
            this.btnDevolverLDevolver = new System.Windows.Forms.Button();
            this.dgvDevolver = new System.Windows.Forms.DataGridView();
            this.lblTituloDevolverL = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevolver)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRentaBuscar
            // 
            this.lblRentaBuscar.AutoSize = true;
            this.lblRentaBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRentaBuscar.Location = new System.Drawing.Point(622, 25);
            this.lblRentaBuscar.Name = "lblRentaBuscar";
            this.lblRentaBuscar.Size = new System.Drawing.Size(63, 20);
            this.lblRentaBuscar.TabIndex = 84;
            this.lblRentaBuscar.Text = "Buscar:";
            // 
            // txtRentaBuscar
            // 
            this.txtRentaBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRentaBuscar.Location = new System.Drawing.Point(691, 23);
            this.txtRentaBuscar.Name = "txtRentaBuscar";
            this.txtRentaBuscar.Size = new System.Drawing.Size(157, 26);
            this.txtRentaBuscar.TabIndex = 83;
            this.txtRentaBuscar.TextChanged += new System.EventHandler(this.txtRentaBuscar_TextChanged);
            // 
            // btnDevolverLCancelar
            // 
            this.btnDevolverLCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevolverLCancelar.Location = new System.Drawing.Point(759, 436);
            this.btnDevolverLCancelar.Name = "btnDevolverLCancelar";
            this.btnDevolverLCancelar.Size = new System.Drawing.Size(90, 30);
            this.btnDevolverLCancelar.TabIndex = 82;
            this.btnDevolverLCancelar.Text = "Cancelar";
            this.btnDevolverLCancelar.UseVisualStyleBackColor = true;
            this.btnDevolverLCancelar.Click += new System.EventHandler(this.btnDevolverLCancelar_Click);
            // 
            // btnDevolverLDevolver
            // 
            this.btnDevolverLDevolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevolverLDevolver.Location = new System.Drawing.Point(663, 436);
            this.btnDevolverLDevolver.Name = "btnDevolverLDevolver";
            this.btnDevolverLDevolver.Size = new System.Drawing.Size(90, 30);
            this.btnDevolverLDevolver.TabIndex = 81;
            this.btnDevolverLDevolver.Text = "Devolver";
            this.btnDevolverLDevolver.UseVisualStyleBackColor = true;
            this.btnDevolverLDevolver.Click += new System.EventHandler(this.btnDevolverLDevolver_Click);
            // 
            // dgvDevolver
            // 
            this.dgvDevolver.AllowUserToAddRows = false;
            this.dgvDevolver.AllowUserToDeleteRows = false;
            this.dgvDevolver.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDevolver.Location = new System.Drawing.Point(12, 78);
            this.dgvDevolver.Name = "dgvDevolver";
            this.dgvDevolver.ReadOnly = true;
            this.dgvDevolver.Size = new System.Drawing.Size(837, 336);
            this.dgvDevolver.TabIndex = 79;
            // 
            // lblTituloDevolverL
            // 
            this.lblTituloDevolverL.AutoSize = true;
            this.lblTituloDevolverL.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloDevolverL.Location = new System.Drawing.Point(12, 9);
            this.lblTituloDevolverL.Name = "lblTituloDevolverL";
            this.lblTituloDevolverL.Size = new System.Drawing.Size(383, 38);
            this.lblTituloDevolverL.TabIndex = 78;
            this.lblTituloDevolverL.Text = "Lista vehiculos rentados";
            // 
            // RentaDevolverLista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 486);
            this.Controls.Add(this.lblRentaBuscar);
            this.Controls.Add(this.txtRentaBuscar);
            this.Controls.Add(this.btnDevolverLCancelar);
            this.Controls.Add(this.btnDevolverLDevolver);
            this.Controls.Add(this.dgvDevolver);
            this.Controls.Add(this.lblTituloDevolverL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RentaDevolverLista";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lista Devolucion";
            this.Load += new System.EventHandler(this.RentaDevolverLista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevolver)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRentaBuscar;
        private System.Windows.Forms.TextBox txtRentaBuscar;
        private System.Windows.Forms.Button btnDevolverLCancelar;
        private System.Windows.Forms.Button btnDevolverLDevolver;
        private System.Windows.Forms.DataGridView dgvDevolver;
        private System.Windows.Forms.Label lblTituloDevolverL;
    }
}