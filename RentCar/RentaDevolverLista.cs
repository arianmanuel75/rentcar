﻿using RentCar.Data;
using RentCar.Service.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar
{
    public partial class RentaDevolverLista : Form
    {
        private readonly ProcesoRentaService _procesoRentaService;
        private readonly ApplicationDbContext _context;

        public RentaDevolverLista()
        {
            InitializeComponent();
            _procesoRentaService = new ProcesoRentaService();
            _context = new ApplicationDbContext();
        }

        private void RentaDevolverLista_Load(object sender, EventArgs e)
        {
            dgvDevolver.DataSource = _procesoRentaService.GetRentado();
        }

        private void btnDevolverLCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnDevolverLDevolver_Click(object sender, EventArgs e)
        {
            var row = dgvDevolver.SelectedCells[0].OwningRow;

            var id = Convert.ToInt32(row.Cells[0].Value);

            var renta = _context.ProcesoRenta.SingleOrDefault(x => x.Id == id);
            
            var vehiculo = Convert.ToInt32(renta.VehiculoId);
            
            //
            
            var vehiculoEstado = _context.Vehiculo.SingleOrDefault(x => x.Id == vehiculo).EstadoId = 1;

            var rentaestado = _context.ProcesoRenta.SingleOrDefault(x => x.Id == id).EstatusRentaId = 2;

            _context.SaveChanges();

            RentaDevolverLista_Load(sender, e);
        }

        private void txtRentaBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtRentaBuscar.Text != string.Empty)
            {
                //Se filtra por comentario, buscar la
                //forma de filtar por los demas campos
                var search = _procesoRentaService.GetRenta().Where(x => x.Comentario.Contains(txtRentaBuscar.Text));
                dgvDevolver.DataSource = search.ToList();
            }
            else
            {
                dgvDevolver.DataSource = _procesoRentaService.GetRenta();
            }
        }
    }
}
