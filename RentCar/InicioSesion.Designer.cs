﻿namespace RentCar
{
    partial class InicioSesion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InicioSesion));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtInicioUsuario = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtInicioContra = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnInicioEntrar = new System.Windows.Forms.Button();
            this.btnInicioCancelar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 130);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // txtInicioUsuario
            // 
            this.txtInicioUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(130)))), ((int)(((byte)(33)))));
            this.txtInicioUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInicioUsuario.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicioUsuario.ForeColor = System.Drawing.Color.White;
            this.txtInicioUsuario.HideSelection = false;
            this.txtInicioUsuario.Location = new System.Drawing.Point(66, 133);
            this.txtInicioUsuario.Name = "txtInicioUsuario";
            this.txtInicioUsuario.Size = new System.Drawing.Size(149, 19);
            this.txtInicioUsuario.TabIndex = 1;
            this.txtInicioUsuario.Text = "Usuario";
            this.txtInicioUsuario.Click += new System.EventHandler(this.txtInicioUsuario_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(82, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(80, 80);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // txtInicioContra
            // 
            this.txtInicioContra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(130)))), ((int)(((byte)(33)))));
            this.txtInicioContra.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInicioContra.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicioContra.ForeColor = System.Drawing.Color.White;
            this.txtInicioContra.HideSelection = false;
            this.txtInicioContra.Location = new System.Drawing.Point(66, 185);
            this.txtInicioContra.Name = "txtInicioContra";
            this.txtInicioContra.Size = new System.Drawing.Size(149, 19);
            this.txtInicioContra.TabIndex = 4;
            this.txtInicioContra.Text = "Contraseña";
            this.txtInicioContra.UseSystemPasswordChar = true;
            this.txtInicioContra.Click += new System.EventHandler(this.txtInicioContra_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(29, 182);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(24, 24);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // btnInicioEntrar
            // 
            this.btnInicioEntrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(68)))), ((int)(((byte)(65)))));
            this.btnInicioEntrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInicioEntrar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInicioEntrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(130)))), ((int)(((byte)(33)))));
            this.btnInicioEntrar.Location = new System.Drawing.Point(29, 243);
            this.btnInicioEntrar.Name = "btnInicioEntrar";
            this.btnInicioEntrar.Size = new System.Drawing.Size(186, 42);
            this.btnInicioEntrar.TabIndex = 5;
            this.btnInicioEntrar.Text = "Iniciar sesion";
            this.btnInicioEntrar.UseVisualStyleBackColor = false;
            this.btnInicioEntrar.Click += new System.EventHandler(this.btnInicioEntrar_Click);
            // 
            // btnInicioCancelar
            // 
            this.btnInicioCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInicioCancelar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInicioCancelar.ForeColor = System.Drawing.Color.White;
            this.btnInicioCancelar.Location = new System.Drawing.Point(29, 291);
            this.btnInicioCancelar.Name = "btnInicioCancelar";
            this.btnInicioCancelar.Size = new System.Drawing.Size(186, 42);
            this.btnInicioCancelar.TabIndex = 6;
            this.btnInicioCancelar.Text = "Cancelar";
            this.btnInicioCancelar.UseVisualStyleBackColor = true;
            this.btnInicioCancelar.Click += new System.EventHandler(this.btnInicioCancelar_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(29, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 1);
            this.label1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(29, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 1);
            this.label2.TabIndex = 8;
            // 
            // InicioSesion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(130)))), ((int)(((byte)(33)))));
            this.ClientSize = new System.Drawing.Size(244, 345);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnInicioCancelar);
            this.Controls.Add(this.btnInicioEntrar);
            this.Controls.Add(this.txtInicioContra);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.txtInicioUsuario);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InicioSesion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inicio de Sesion";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtInicioUsuario;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtInicioContra;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnInicioEntrar;
        private System.Windows.Forms.Button btnInicioCancelar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}