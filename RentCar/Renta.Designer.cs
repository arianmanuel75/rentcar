﻿namespace RentCar
{
    partial class Renta
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Renta));
            this.lblTituloRenta = new System.Windows.Forms.Label();
            this.lblDevolverVehiculo = new System.Windows.Forms.Label();
            this.lblRentar = new System.Windows.Forms.Label();
            this.btnRentarVehiculo = new System.Windows.Forms.Button();
            this.btnDevolverVehiculo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnListaDevolucion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTituloRenta
            // 
            this.lblTituloRenta.AutoSize = true;
            this.lblTituloRenta.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloRenta.Location = new System.Drawing.Point(3, 0);
            this.lblTituloRenta.Name = "lblTituloRenta";
            this.lblTituloRenta.Size = new System.Drawing.Size(106, 38);
            this.lblTituloRenta.TabIndex = 3;
            this.lblTituloRenta.Text = "Renta";
            // 
            // lblDevolverVehiculo
            // 
            this.lblDevolverVehiculo.AutoSize = true;
            this.lblDevolverVehiculo.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDevolverVehiculo.Location = new System.Drawing.Point(548, 122);
            this.lblDevolverVehiculo.Name = "lblDevolverVehiculo";
            this.lblDevolverVehiculo.Size = new System.Drawing.Size(146, 23);
            this.lblDevolverVehiculo.TabIndex = 12;
            this.lblDevolverVehiculo.Text = "Lista de Rentas";
            // 
            // lblRentar
            // 
            this.lblRentar.AutoSize = true;
            this.lblRentar.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRentar.Location = new System.Drawing.Point(95, 122);
            this.lblRentar.Name = "lblRentar";
            this.lblRentar.Size = new System.Drawing.Size(155, 23);
            this.lblRentar.TabIndex = 11;
            this.lblRentar.Text = "Rentar Vehiculo";
            // 
            // btnRentarVehiculo
            // 
            this.btnRentarVehiculo.BackColor = System.Drawing.Color.White;
            this.btnRentarVehiculo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRentarVehiculo.Image = ((System.Drawing.Image)(resources.GetObject("btnRentarVehiculo.Image")));
            this.btnRentarVehiculo.Location = new System.Drawing.Point(99, 148);
            this.btnRentarVehiculo.Name = "btnRentarVehiculo";
            this.btnRentarVehiculo.Size = new System.Drawing.Size(144, 144);
            this.btnRentarVehiculo.TabIndex = 10;
            this.btnRentarVehiculo.UseVisualStyleBackColor = false;
            this.btnRentarVehiculo.Click += new System.EventHandler(this.btnRentarVehiculo_Click);
            // 
            // btnDevolverVehiculo
            // 
            this.btnDevolverVehiculo.BackColor = System.Drawing.Color.White;
            this.btnDevolverVehiculo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDevolverVehiculo.Image = ((System.Drawing.Image)(resources.GetObject("btnDevolverVehiculo.Image")));
            this.btnDevolverVehiculo.Location = new System.Drawing.Point(549, 148);
            this.btnDevolverVehiculo.Name = "btnDevolverVehiculo";
            this.btnDevolverVehiculo.Size = new System.Drawing.Size(144, 144);
            this.btnDevolverVehiculo.TabIndex = 9;
            this.btnDevolverVehiculo.UseVisualStyleBackColor = false;
            this.btnDevolverVehiculo.Click += new System.EventHandler(this.btnDevolverVehiculo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(300, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 23);
            this.label1.TabIndex = 14;
            this.label1.Text = "Lista para devolucion";
            // 
            // btnListaDevolucion
            // 
            this.btnListaDevolucion.BackColor = System.Drawing.Color.White;
            this.btnListaDevolucion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListaDevolucion.Image = ((System.Drawing.Image)(resources.GetObject("btnListaDevolucion.Image")));
            this.btnListaDevolucion.Location = new System.Drawing.Point(330, 148);
            this.btnListaDevolucion.Name = "btnListaDevolucion";
            this.btnListaDevolucion.Size = new System.Drawing.Size(144, 144);
            this.btnListaDevolucion.TabIndex = 13;
            this.btnListaDevolucion.UseVisualStyleBackColor = false;
            this.btnListaDevolucion.Click += new System.EventHandler(this.btnListaDevolucion_Click);
            // 
            // Renta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnListaDevolucion);
            this.Controls.Add(this.lblDevolverVehiculo);
            this.Controls.Add(this.lblRentar);
            this.Controls.Add(this.btnRentarVehiculo);
            this.Controls.Add(this.btnDevolverVehiculo);
            this.Controls.Add(this.lblTituloRenta);
            this.Name = "Renta";
            this.Size = new System.Drawing.Size(818, 478);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTituloRenta;
        private System.Windows.Forms.Label lblDevolverVehiculo;
        private System.Windows.Forms.Label lblRentar;
        private System.Windows.Forms.Button btnRentarVehiculo;
        private System.Windows.Forms.Button btnDevolverVehiculo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnListaDevolucion;
    }
}
