﻿namespace RentCar
{
    partial class Reporte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reporte));
            this.dgvReporte = new System.Windows.Forms.DataGridView();
            this.lblTituloReporte = new System.Windows.Forms.Label();
            this.btnReporteLCancelar = new System.Windows.Forms.Button();
            this.lblReporteBuscar = new System.Windows.Forms.Label();
            this.txtReporteBuscar = new System.Windows.Forms.TextBox();
            this.btnReporteExportar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporte)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvReporte
            // 
            this.dgvReporte.AllowUserToAddRows = false;
            this.dgvReporte.AllowUserToDeleteRows = false;
            this.dgvReporte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReporte.Location = new System.Drawing.Point(12, 78);
            this.dgvReporte.Name = "dgvReporte";
            this.dgvReporte.ReadOnly = true;
            this.dgvReporte.Size = new System.Drawing.Size(837, 336);
            this.dgvReporte.TabIndex = 27;
            // 
            // lblTituloReporte
            // 
            this.lblTituloReporte.AutoSize = true;
            this.lblTituloReporte.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloReporte.Location = new System.Drawing.Point(12, 9);
            this.lblTituloReporte.Name = "lblTituloReporte";
            this.lblTituloReporte.Size = new System.Drawing.Size(137, 38);
            this.lblTituloReporte.TabIndex = 26;
            this.lblTituloReporte.Text = "Reporte";
            // 
            // btnReporteLCancelar
            // 
            this.btnReporteLCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporteLCancelar.Location = new System.Drawing.Point(759, 436);
            this.btnReporteLCancelar.Name = "btnReporteLCancelar";
            this.btnReporteLCancelar.Size = new System.Drawing.Size(90, 30);
            this.btnReporteLCancelar.TabIndex = 30;
            this.btnReporteLCancelar.Text = "Cancelar";
            this.btnReporteLCancelar.UseVisualStyleBackColor = true;
            this.btnReporteLCancelar.Click += new System.EventHandler(this.btnReporteLCancelar_Click);
            // 
            // lblReporteBuscar
            // 
            this.lblReporteBuscar.AutoSize = true;
            this.lblReporteBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReporteBuscar.Location = new System.Drawing.Point(623, 28);
            this.lblReporteBuscar.Name = "lblReporteBuscar";
            this.lblReporteBuscar.Size = new System.Drawing.Size(63, 20);
            this.lblReporteBuscar.TabIndex = 39;
            this.lblReporteBuscar.Text = "Buscar:";
            // 
            // txtReporteBuscar
            // 
            this.txtReporteBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReporteBuscar.Location = new System.Drawing.Point(692, 25);
            this.txtReporteBuscar.Name = "txtReporteBuscar";
            this.txtReporteBuscar.Size = new System.Drawing.Size(157, 26);
            this.txtReporteBuscar.TabIndex = 38;
            this.txtReporteBuscar.TextChanged += new System.EventHandler(this.txtReporteBuscar_TextChanged);
            // 
            // btnReporteExportar
            // 
            this.btnReporteExportar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporteExportar.Location = new System.Drawing.Point(663, 436);
            this.btnReporteExportar.Name = "btnReporteExportar";
            this.btnReporteExportar.Size = new System.Drawing.Size(90, 30);
            this.btnReporteExportar.TabIndex = 40;
            this.btnReporteExportar.Text = "Exportar";
            this.btnReporteExportar.UseVisualStyleBackColor = true;
            this.btnReporteExportar.Click += new System.EventHandler(this.btnReporteExportar_Click);
            // 
            // Reporte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 486);
            this.Controls.Add(this.btnReporteExportar);
            this.Controls.Add(this.lblReporteBuscar);
            this.Controls.Add(this.txtReporteBuscar);
            this.Controls.Add(this.btnReporteLCancelar);
            this.Controls.Add(this.dgvReporte);
            this.Controls.Add(this.lblTituloReporte);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Reporte";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reporte";
            this.Load += new System.EventHandler(this.Reporte_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporte)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvReporte;
        private System.Windows.Forms.Label lblTituloReporte;
        private System.Windows.Forms.Button btnReporteLCancelar;
        private System.Windows.Forms.Label lblReporteBuscar;
        private System.Windows.Forms.TextBox txtReporteBuscar;
        private System.Windows.Forms.Button btnReporteExportar;
    }
}