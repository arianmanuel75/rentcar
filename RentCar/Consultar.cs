﻿using RentCar.Service.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar
{
    public partial class Consultar : Form
    {
        private readonly ProcesoRentaService _procesoRentaService;
        public Consultar()
        {
            InitializeComponent();
            _procesoRentaService = new ProcesoRentaService();
        }
        private void Consultar_Load(object sender, EventArgs e)
        {

            dgvConsulta.DataSource = _procesoRentaService.GetConsulta();
        }

        private void btnConsultaLCancelar_Click(object sender, EventArgs e)
        {
            Dispose(); ;
        }

        private void txtConsultaBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtConsultaBuscar.Text != string.Empty)
            {
                var search = _procesoRentaService.GetConsulta().Where(x => x.VehiculoDescripcion.Contains(txtConsultaBuscar.Text));
                dgvConsulta.DataSource = search.ToList();
            }
            else
            {
                dgvConsulta.DataSource = _procesoRentaService.GetConsulta();
            }
        }

        private void btnConsultaExportar_Click(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today;

            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel.Worksheet worsheet = null;
            worsheet = workbook.Sheets["Hoja1"];
            worsheet = workbook.ActiveSheet;
            worsheet.Name = "RentCarConsulta";

            for (int i = 1; i < dgvConsulta.Columns.Count + 1; i++)
            {
                worsheet.Cells[1, i] = dgvConsulta.Columns[i - 1].HeaderText;
            }

            for (int i = 0; i < dgvConsulta.Rows.Count; i++)
            {
                for (int j = 0; j < dgvConsulta.Columns.Count; j++)
                {
                    worsheet.Cells[i + 2, j + 1] = dgvConsulta.Rows[i].Cells[j].Value.ToString();
                }
            }

            var saveFileDialoge = new SaveFileDialog();
            saveFileDialoge.FileName = "Consulta " + today.ToString("dd-MM-yyyy");
            saveFileDialoge.DefaultExt = ".xlsx";
            if (saveFileDialoge.ShowDialog() == DialogResult.OK)
            {
                workbook.SaveAs(saveFileDialoge.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            app.Quit();
        }
    }
}
