﻿namespace RentCar
{
    partial class Inspeccionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inspeccionar));
            this.lblInspecFechaActual = new System.Windows.Forms.Label();
            this.btnClienteCancelar = new System.Windows.Forms.Button();
            this.btnClienteGuardar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.chkInspecGoma4 = new System.Windows.Forms.CheckBox();
            this.chkInspecGoma3 = new System.Windows.Forms.CheckBox();
            this.chkInspecGoma2 = new System.Windows.Forms.CheckBox();
            this.lblInspecFecha = new System.Windows.Forms.Label();
            this.lblInspecCliente = new System.Windows.Forms.Label();
            this.lblInspecVehiculo = new System.Windows.Forms.Label();
            this.cbInspecCliente = new System.Windows.Forms.ComboBox();
            this.cbInspecVehiculo = new System.Windows.Forms.ComboBox();
            this.lblInspecEstado = new System.Windows.Forms.Label();
            this.chkInspecEstado = new System.Windows.Forms.CheckBox();
            this.lblInspecEstadoGoma = new System.Windows.Forms.Label();
            this.chkInspecGoma1 = new System.Windows.Forms.CheckBox();
            this.lblInspectRotura = new System.Windows.Forms.Label();
            this.chkInspectRotCristal = new System.Windows.Forms.CheckBox();
            this.lblInspectGato = new System.Windows.Forms.Label();
            this.chkInspectGato = new System.Windows.Forms.CheckBox();
            this.lblInspectRespaldo = new System.Windows.Forms.Label();
            this.chkInspectRespaldo = new System.Windows.Forms.CheckBox();
            this.lblInspecRalladura = new System.Windows.Forms.Label();
            this.lblInspecCombustible = new System.Windows.Forms.Label();
            this.chkInspectComb3 = new System.Windows.Forms.CheckBox();
            this.chkInspectComb2 = new System.Windows.Forms.CheckBox();
            this.chkInspectComb4 = new System.Windows.Forms.CheckBox();
            this.chkInspectComb1 = new System.Windows.Forms.CheckBox();
            this.chkInspecRalladura = new System.Windows.Forms.CheckBox();
            this.lblTituloInspeccion = new System.Windows.Forms.Label();
            this.lblInspecId = new System.Windows.Forms.Label();
            this.lblInspectEmpleado = new System.Windows.Forms.Label();
            this.lblInspecEmpConectado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblInspecFechaActual
            // 
            this.lblInspecFechaActual.AutoSize = true;
            this.lblInspecFechaActual.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecFechaActual.Location = new System.Drawing.Point(676, 130);
            this.lblInspecFechaActual.Name = "lblInspecFechaActual";
            this.lblInspecFechaActual.Size = new System.Drawing.Size(97, 18);
            this.lblInspecFechaActual.TabIndex = 68;
            this.lblInspecFechaActual.Text = "Fecha actual";
            // 
            // btnClienteCancelar
            // 
            this.btnClienteCancelar.BackColor = System.Drawing.SystemColors.Control;
            this.btnClienteCancelar.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnClienteCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnClienteCancelar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClienteCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteCancelar.Location = new System.Drawing.Point(693, 453);
            this.btnClienteCancelar.Name = "btnClienteCancelar";
            this.btnClienteCancelar.Size = new System.Drawing.Size(95, 30);
            this.btnClienteCancelar.TabIndex = 67;
            this.btnClienteCancelar.Text = "Cancelar";
            this.btnClienteCancelar.UseVisualStyleBackColor = false;
            this.btnClienteCancelar.Click += new System.EventHandler(this.btnClienteCancelar_Click);
            // 
            // btnClienteGuardar
            // 
            this.btnClienteGuardar.BackColor = System.Drawing.SystemColors.Control;
            this.btnClienteGuardar.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnClienteGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnClienteGuardar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClienteGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteGuardar.Location = new System.Drawing.Point(579, 453);
            this.btnClienteGuardar.Name = "btnClienteGuardar";
            this.btnClienteGuardar.Size = new System.Drawing.Size(95, 30);
            this.btnClienteGuardar.TabIndex = 66;
            this.btnClienteGuardar.Text = "Guardar";
            this.btnClienteGuardar.UseVisualStyleBackColor = false;
            this.btnClienteGuardar.Click += new System.EventHandler(this.btnClienteGuardar_Click);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(1, 436);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(799, 2);
            this.label1.TabIndex = 65;
            // 
            // chkInspecGoma4
            // 
            this.chkInspecGoma4.AutoSize = true;
            this.chkInspecGoma4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspecGoma4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspecGoma4.Location = new System.Drawing.Point(651, 367);
            this.chkInspecGoma4.Name = "chkInspecGoma4";
            this.chkInspecGoma4.Size = new System.Drawing.Size(141, 22);
            this.chkInspecGoma4.TabIndex = 64;
            this.chkInspecGoma4.Text = "Trasera Derecha";
            this.chkInspecGoma4.UseVisualStyleBackColor = true;
            // 
            // chkInspecGoma3
            // 
            this.chkInspecGoma3.AutoSize = true;
            this.chkInspecGoma3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspecGoma3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspecGoma3.Location = new System.Drawing.Point(501, 367);
            this.chkInspecGoma3.Name = "chkInspecGoma3";
            this.chkInspecGoma3.Size = new System.Drawing.Size(144, 22);
            this.chkInspecGoma3.TabIndex = 63;
            this.chkInspecGoma3.Text = "Trasera Izquierda";
            this.chkInspecGoma3.UseVisualStyleBackColor = true;
            // 
            // chkInspecGoma2
            // 
            this.chkInspecGoma2.AutoSize = true;
            this.chkInspecGoma2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspecGoma2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspecGoma2.Location = new System.Drawing.Point(339, 367);
            this.chkInspecGoma2.Name = "chkInspecGoma2";
            this.chkInspecGoma2.Size = new System.Drawing.Size(156, 22);
            this.chkInspecGoma2.TabIndex = 62;
            this.chkInspecGoma2.Text = "Delantera Derecha";
            this.chkInspecGoma2.UseVisualStyleBackColor = true;
            // 
            // lblInspecFecha
            // 
            this.lblInspecFecha.AutoSize = true;
            this.lblInspecFecha.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecFecha.Location = new System.Drawing.Point(608, 130);
            this.lblInspecFecha.Name = "lblInspecFecha";
            this.lblInspecFecha.Size = new System.Drawing.Size(62, 19);
            this.lblInspecFecha.TabIndex = 61;
            this.lblInspecFecha.Text = "Fecha:";
            // 
            // lblInspecCliente
            // 
            this.lblInspecCliente.AutoSize = true;
            this.lblInspecCliente.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecCliente.Location = new System.Drawing.Point(327, 129);
            this.lblInspecCliente.Name = "lblInspecCliente";
            this.lblInspecCliente.Size = new System.Drawing.Size(68, 19);
            this.lblInspecCliente.TabIndex = 60;
            this.lblInspecCliente.Text = "Cliente:";
            // 
            // lblInspecVehiculo
            // 
            this.lblInspecVehiculo.AutoSize = true;
            this.lblInspecVehiculo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecVehiculo.Location = new System.Drawing.Point(20, 130);
            this.lblInspecVehiculo.Name = "lblInspecVehiculo";
            this.lblInspecVehiculo.Size = new System.Drawing.Size(81, 19);
            this.lblInspecVehiculo.TabIndex = 59;
            this.lblInspecVehiculo.Text = "Vehiculo:";
            // 
            // cbInspecCliente
            // 
            this.cbInspecCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInspecCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInspecCliente.FormattingEnabled = true;
            this.cbInspecCliente.Location = new System.Drawing.Point(401, 127);
            this.cbInspecCliente.Name = "cbInspecCliente";
            this.cbInspecCliente.Size = new System.Drawing.Size(176, 26);
            this.cbInspecCliente.TabIndex = 58;
            // 
            // cbInspecVehiculo
            // 
            this.cbInspecVehiculo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInspecVehiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInspecVehiculo.FormattingEnabled = true;
            this.cbInspecVehiculo.Location = new System.Drawing.Point(107, 127);
            this.cbInspecVehiculo.Name = "cbInspecVehiculo";
            this.cbInspecVehiculo.Size = new System.Drawing.Size(207, 26);
            this.cbInspecVehiculo.TabIndex = 57;
            // 
            // lblInspecEstado
            // 
            this.lblInspecEstado.AutoSize = true;
            this.lblInspecEstado.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecEstado.Location = new System.Drawing.Point(20, 403);
            this.lblInspecEstado.Name = "lblInspecEstado";
            this.lblInspecEstado.Size = new System.Drawing.Size(69, 19);
            this.lblInspecEstado.TabIndex = 56;
            this.lblInspecEstado.Text = "Estado:";
            // 
            // chkInspecEstado
            // 
            this.chkInspecEstado.AutoSize = true;
            this.chkInspecEstado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspecEstado.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspecEstado.Location = new System.Drawing.Point(95, 407);
            this.chkInspecEstado.Name = "chkInspecEstado";
            this.chkInspecEstado.Size = new System.Drawing.Size(12, 11);
            this.chkInspecEstado.TabIndex = 55;
            this.chkInspecEstado.UseVisualStyleBackColor = true;
            // 
            // lblInspecEstadoGoma
            // 
            this.lblInspecEstadoGoma.AutoSize = true;
            this.lblInspecEstadoGoma.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecEstadoGoma.Location = new System.Drawing.Point(20, 367);
            this.lblInspecEstadoGoma.Name = "lblInspecEstadoGoma";
            this.lblInspecEstadoGoma.Size = new System.Drawing.Size(148, 19);
            this.lblInspecEstadoGoma.TabIndex = 54;
            this.lblInspecEstadoGoma.Text = "Estado de gomas:";
            // 
            // chkInspecGoma1
            // 
            this.chkInspecGoma1.AutoSize = true;
            this.chkInspecGoma1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspecGoma1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspecGoma1.Location = new System.Drawing.Point(174, 367);
            this.chkInspecGoma1.Name = "chkInspecGoma1";
            this.chkInspecGoma1.Size = new System.Drawing.Size(159, 22);
            this.chkInspecGoma1.TabIndex = 53;
            this.chkInspecGoma1.Text = "Delantera Izquierda";
            this.chkInspecGoma1.UseVisualStyleBackColor = true;
            // 
            // lblInspectRotura
            // 
            this.lblInspectRotura.AutoSize = true;
            this.lblInspectRotura.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspectRotura.Location = new System.Drawing.Point(20, 331);
            this.lblInspectRotura.Name = "lblInspectRotura";
            this.lblInspectRotura.Size = new System.Drawing.Size(198, 19);
            this.lblInspectRotura.TabIndex = 52;
            this.lblInspectRotura.Text = "Tiene Roturas de Cristal:";
            // 
            // chkInspectRotCristal
            // 
            this.chkInspectRotCristal.AutoSize = true;
            this.chkInspectRotCristal.Location = new System.Drawing.Point(224, 335);
            this.chkInspectRotCristal.Name = "chkInspectRotCristal";
            this.chkInspectRotCristal.Size = new System.Drawing.Size(15, 14);
            this.chkInspectRotCristal.TabIndex = 1;
            // 
            // lblInspectGato
            // 
            this.lblInspectGato.AutoSize = true;
            this.lblInspectGato.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspectGato.Location = new System.Drawing.Point(20, 295);
            this.lblInspectGato.Name = "lblInspectGato";
            this.lblInspectGato.Size = new System.Drawing.Size(97, 19);
            this.lblInspectGato.TabIndex = 50;
            this.lblInspectGato.Text = "Tiene Gato:";
            // 
            // chkInspectGato
            // 
            this.chkInspectGato.AutoSize = true;
            this.chkInspectGato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspectGato.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspectGato.Location = new System.Drawing.Point(123, 300);
            this.chkInspectGato.Name = "chkInspectGato";
            this.chkInspectGato.Size = new System.Drawing.Size(12, 11);
            this.chkInspectGato.TabIndex = 49;
            this.chkInspectGato.UseVisualStyleBackColor = true;
            // 
            // lblInspectRespaldo
            // 
            this.lblInspectRespaldo.AutoSize = true;
            this.lblInspectRespaldo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspectRespaldo.Location = new System.Drawing.Point(20, 259);
            this.lblInspectRespaldo.Name = "lblInspectRespaldo";
            this.lblInspectRespaldo.Size = new System.Drawing.Size(198, 19);
            this.lblInspectRespaldo.TabIndex = 48;
            this.lblInspectRespaldo.Text = "Tiene goma de respaldo:";
            // 
            // chkInspectRespaldo
            // 
            this.chkInspectRespaldo.AutoSize = true;
            this.chkInspectRespaldo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspectRespaldo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspectRespaldo.Location = new System.Drawing.Point(224, 264);
            this.chkInspectRespaldo.Name = "chkInspectRespaldo";
            this.chkInspectRespaldo.Size = new System.Drawing.Size(12, 11);
            this.chkInspectRespaldo.TabIndex = 47;
            this.chkInspectRespaldo.UseVisualStyleBackColor = true;
            // 
            // lblInspecRalladura
            // 
            this.lblInspecRalladura.AutoSize = true;
            this.lblInspecRalladura.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecRalladura.Location = new System.Drawing.Point(20, 187);
            this.lblInspecRalladura.Name = "lblInspecRalladura";
            this.lblInspecRalladura.Size = new System.Drawing.Size(143, 19);
            this.lblInspecRalladura.TabIndex = 46;
            this.lblInspecRalladura.Text = "Tiene Ralladuras:";
            // 
            // lblInspecCombustible
            // 
            this.lblInspecCombustible.AutoSize = true;
            this.lblInspecCombustible.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecCombustible.Location = new System.Drawing.Point(20, 223);
            this.lblInspecCombustible.Name = "lblInspecCombustible";
            this.lblInspecCombustible.Size = new System.Drawing.Size(208, 19);
            this.lblInspecCombustible.TabIndex = 45;
            this.lblInspecCombustible.Text = "Cantidad de Combustible:";
            // 
            // chkInspectComb3
            // 
            this.chkInspectComb3.AutoSize = true;
            this.chkInspectComb3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspectComb3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspectComb3.Location = new System.Drawing.Point(338, 223);
            this.chkInspectComb3.Name = "chkInspectComb3";
            this.chkInspectComb3.Size = new System.Drawing.Size(46, 22);
            this.chkInspectComb3.TabIndex = 44;
            this.chkInspectComb3.Text = "3/4";
            this.chkInspectComb3.UseVisualStyleBackColor = true;
            // 
            // chkInspectComb2
            // 
            this.chkInspectComb2.AutoSize = true;
            this.chkInspectComb2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspectComb2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspectComb2.Location = new System.Drawing.Point(286, 223);
            this.chkInspectComb2.Name = "chkInspectComb2";
            this.chkInspectComb2.Size = new System.Drawing.Size(46, 22);
            this.chkInspectComb2.TabIndex = 43;
            this.chkInspectComb2.Text = "1/2";
            this.chkInspectComb2.UseVisualStyleBackColor = true;
            // 
            // chkInspectComb4
            // 
            this.chkInspectComb4.AutoSize = true;
            this.chkInspectComb4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspectComb4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspectComb4.Location = new System.Drawing.Point(390, 223);
            this.chkInspectComb4.Name = "chkInspectComb4";
            this.chkInspectComb4.Size = new System.Drawing.Size(62, 22);
            this.chkInspectComb4.TabIndex = 42;
            this.chkInspectComb4.Text = "Lleno";
            this.chkInspectComb4.UseVisualStyleBackColor = true;
            // 
            // chkInspectComb1
            // 
            this.chkInspectComb1.AutoSize = true;
            this.chkInspectComb1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspectComb1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspectComb1.Location = new System.Drawing.Point(234, 223);
            this.chkInspectComb1.Name = "chkInspectComb1";
            this.chkInspectComb1.Size = new System.Drawing.Size(46, 22);
            this.chkInspectComb1.TabIndex = 41;
            this.chkInspectComb1.Text = "1/4";
            this.chkInspectComb1.UseVisualStyleBackColor = true;
            // 
            // chkInspecRalladura
            // 
            this.chkInspecRalladura.AutoSize = true;
            this.chkInspecRalladura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkInspecRalladura.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInspecRalladura.Location = new System.Drawing.Point(169, 192);
            this.chkInspecRalladura.Name = "chkInspecRalladura";
            this.chkInspecRalladura.Size = new System.Drawing.Size(12, 11);
            this.chkInspecRalladura.TabIndex = 40;
            this.chkInspecRalladura.UseVisualStyleBackColor = true;
            // 
            // lblTituloInspeccion
            // 
            this.lblTituloInspeccion.AutoSize = true;
            this.lblTituloInspeccion.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloInspeccion.Location = new System.Drawing.Point(12, 9);
            this.lblTituloInspeccion.Name = "lblTituloInspeccion";
            this.lblTituloInspeccion.Size = new System.Drawing.Size(187, 38);
            this.lblTituloInspeccion.TabIndex = 39;
            this.lblTituloInspeccion.Text = "Inspeccion";
            // 
            // lblInspecId
            // 
            this.lblInspecId.AutoSize = true;
            this.lblInspecId.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecId.Location = new System.Drawing.Point(602, 54);
            this.lblInspecId.Name = "lblInspecId";
            this.lblInspecId.Size = new System.Drawing.Size(171, 29);
            this.lblInspecId.TabIndex = 69;
            this.lblInspecId.Text = "Inspeccion ID";
            // 
            // lblInspectEmpleado
            // 
            this.lblInspectEmpleado.AutoSize = true;
            this.lblInspectEmpleado.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspectEmpleado.Location = new System.Drawing.Point(506, 184);
            this.lblInspectEmpleado.Name = "lblInspectEmpleado";
            this.lblInspectEmpleado.Size = new System.Drawing.Size(92, 19);
            this.lblInspectEmpleado.TabIndex = 70;
            this.lblInspectEmpleado.Text = "Empleado:";
            // 
            // lblInspecEmpConectado
            // 
            this.lblInspecEmpConectado.AutoSize = true;
            this.lblInspecEmpConectado.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecEmpConectado.Location = new System.Drawing.Point(604, 184);
            this.lblInspecEmpConectado.Name = "lblInspecEmpConectado";
            this.lblInspecEmpConectado.Size = new System.Drawing.Size(169, 18);
            this.lblInspecEmpConectado.TabIndex = 71;
            this.lblInspecEmpConectado.Text = "\"Empleado conectado\"";
            // 
            // Inspeccionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 493);
            this.Controls.Add(this.lblInspecEmpConectado);
            this.Controls.Add(this.lblInspectEmpleado);
            this.Controls.Add(this.lblInspecId);
            this.Controls.Add(this.lblInspecFechaActual);
            this.Controls.Add(this.btnClienteCancelar);
            this.Controls.Add(this.btnClienteGuardar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkInspecGoma4);
            this.Controls.Add(this.chkInspecGoma3);
            this.Controls.Add(this.chkInspecGoma2);
            this.Controls.Add(this.lblInspecFecha);
            this.Controls.Add(this.lblInspecCliente);
            this.Controls.Add(this.lblInspecVehiculo);
            this.Controls.Add(this.cbInspecCliente);
            this.Controls.Add(this.cbInspecVehiculo);
            this.Controls.Add(this.lblInspecEstado);
            this.Controls.Add(this.chkInspecEstado);
            this.Controls.Add(this.lblInspecEstadoGoma);
            this.Controls.Add(this.chkInspecGoma1);
            this.Controls.Add(this.lblInspectRotura);
            this.Controls.Add(this.chkInspectRotCristal);
            this.Controls.Add(this.lblInspectGato);
            this.Controls.Add(this.chkInspectGato);
            this.Controls.Add(this.lblInspectRespaldo);
            this.Controls.Add(this.chkInspectRespaldo);
            this.Controls.Add(this.lblInspecRalladura);
            this.Controls.Add(this.lblInspecCombustible);
            this.Controls.Add(this.chkInspectComb3);
            this.Controls.Add(this.chkInspectComb2);
            this.Controls.Add(this.chkInspectComb4);
            this.Controls.Add(this.chkInspectComb1);
            this.Controls.Add(this.chkInspecRalladura);
            this.Controls.Add(this.lblTituloInspeccion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Inspeccionar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inspeccion";
            this.Load += new System.EventHandler(this.Inspeccionar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInspecFechaActual;
        private System.Windows.Forms.Button btnClienteCancelar;
        private System.Windows.Forms.Button btnClienteGuardar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkInspecGoma4;
        private System.Windows.Forms.CheckBox chkInspecGoma3;
        private System.Windows.Forms.CheckBox chkInspecGoma2;
        private System.Windows.Forms.Label lblInspecFecha;
        private System.Windows.Forms.Label lblInspecCliente;
        private System.Windows.Forms.Label lblInspecVehiculo;
        private System.Windows.Forms.ComboBox cbInspecCliente;
        private System.Windows.Forms.ComboBox cbInspecVehiculo;
        private System.Windows.Forms.Label lblInspecEstado;
        private System.Windows.Forms.CheckBox chkInspecEstado;
        private System.Windows.Forms.Label lblInspecEstadoGoma;
        private System.Windows.Forms.CheckBox chkInspecGoma1;
        private System.Windows.Forms.Label lblInspectRotura;
        private System.Windows.Forms.CheckBox chkInspectRotCristal;
        private System.Windows.Forms.Label lblInspectGato;
        private System.Windows.Forms.CheckBox chkInspectGato;
        private System.Windows.Forms.Label lblInspectRespaldo;
        private System.Windows.Forms.CheckBox chkInspectRespaldo;
        private System.Windows.Forms.Label lblInspecRalladura;
        private System.Windows.Forms.Label lblInspecCombustible;
        private System.Windows.Forms.CheckBox chkInspectComb3;
        private System.Windows.Forms.CheckBox chkInspectComb2;
        private System.Windows.Forms.CheckBox chkInspectComb4;
        private System.Windows.Forms.CheckBox chkInspectComb1;
        private System.Windows.Forms.CheckBox chkInspecRalladura;
        private System.Windows.Forms.Label lblTituloInspeccion;
        private System.Windows.Forms.Label lblInspecId;
        private System.Windows.Forms.Label lblInspectEmpleado;
        private System.Windows.Forms.Label lblInspecEmpConectado;
    }
}