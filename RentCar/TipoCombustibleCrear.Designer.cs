﻿namespace RentCar
{
    partial class TipoCombustibleCrear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TipoCombustibleCrear));
            this.lblTipoCombustibleDescripcion = new System.Windows.Forms.Label();
            this.txtTipoCombustibleDescripcion = new System.Windows.Forms.TextBox();
            this.btnTipoCombustibleEliminar = new System.Windows.Forms.Button();
            this.btnTipoCombustibleEditar = new System.Windows.Forms.Button();
            this.btnTipoCombustibleGuardar = new System.Windows.Forms.Button();
            this.lblTipoCombustibleTitulo = new System.Windows.Forms.Label();
            this.dgvTipoCombustible = new System.Windows.Forms.DataGridView();
            this.btnTipoCombustibleCancelar = new System.Windows.Forms.Button();
            this.lblTipoCombustibleId = new System.Windows.Forms.Label();
            this.lblCombustibleBuscar = new System.Windows.Forms.Label();
            this.txtCombustibleBuscar = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTipoCombustible)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTipoCombustibleDescripcion
            // 
            this.lblTipoCombustibleDescripcion.AutoSize = true;
            this.lblTipoCombustibleDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoCombustibleDescripcion.Location = new System.Drawing.Point(339, 101);
            this.lblTipoCombustibleDescripcion.Name = "lblTipoCombustibleDescripcion";
            this.lblTipoCombustibleDescripcion.Size = new System.Drawing.Size(115, 24);
            this.lblTipoCombustibleDescripcion.TabIndex = 38;
            this.lblTipoCombustibleDescripcion.Text = "Descripcion:";
            // 
            // txtTipoCombustibleDescripcion
            // 
            this.txtTipoCombustibleDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTipoCombustibleDescripcion.Location = new System.Drawing.Point(343, 128);
            this.txtTipoCombustibleDescripcion.Name = "txtTipoCombustibleDescripcion";
            this.txtTipoCombustibleDescripcion.Size = new System.Drawing.Size(240, 26);
            this.txtTipoCombustibleDescripcion.TabIndex = 37;
            // 
            // btnTipoCombustibleEliminar
            // 
            this.btnTipoCombustibleEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoCombustibleEliminar.Location = new System.Drawing.Point(422, 250);
            this.btnTipoCombustibleEliminar.Name = "btnTipoCombustibleEliminar";
            this.btnTipoCombustibleEliminar.Size = new System.Drawing.Size(90, 30);
            this.btnTipoCombustibleEliminar.TabIndex = 36;
            this.btnTipoCombustibleEliminar.Text = "Eliminar";
            this.btnTipoCombustibleEliminar.UseVisualStyleBackColor = true;
            this.btnTipoCombustibleEliminar.Click += new System.EventHandler(this.btnTipoCombustibleEliminar_Click);
            // 
            // btnTipoCombustibleEditar
            // 
            this.btnTipoCombustibleEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoCombustibleEditar.Location = new System.Drawing.Point(422, 214);
            this.btnTipoCombustibleEditar.Name = "btnTipoCombustibleEditar";
            this.btnTipoCombustibleEditar.Size = new System.Drawing.Size(90, 30);
            this.btnTipoCombustibleEditar.TabIndex = 35;
            this.btnTipoCombustibleEditar.Text = "Editar";
            this.btnTipoCombustibleEditar.UseVisualStyleBackColor = true;
            this.btnTipoCombustibleEditar.Click += new System.EventHandler(this.btnTipoCombustibleEditar_Click);
            // 
            // btnTipoCombustibleGuardar
            // 
            this.btnTipoCombustibleGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoCombustibleGuardar.Location = new System.Drawing.Point(422, 178);
            this.btnTipoCombustibleGuardar.Name = "btnTipoCombustibleGuardar";
            this.btnTipoCombustibleGuardar.Size = new System.Drawing.Size(90, 30);
            this.btnTipoCombustibleGuardar.TabIndex = 34;
            this.btnTipoCombustibleGuardar.Text = "Guardar";
            this.btnTipoCombustibleGuardar.UseVisualStyleBackColor = true;
            this.btnTipoCombustibleGuardar.Click += new System.EventHandler(this.btnTipoCombustibleGuardar_Click);
            // 
            // lblTipoCombustibleTitulo
            // 
            this.lblTipoCombustibleTitulo.AutoSize = true;
            this.lblTipoCombustibleTitulo.BackColor = System.Drawing.SystemColors.Control;
            this.lblTipoCombustibleTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoCombustibleTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTipoCombustibleTitulo.Name = "lblTipoCombustibleTitulo";
            this.lblTipoCombustibleTitulo.Size = new System.Drawing.Size(455, 37);
            this.lblTipoCombustibleTitulo.TabIndex = 33;
            this.lblTipoCombustibleTitulo.Text = "Agregar Tipo de combustible";
            // 
            // dgvTipoCombustible
            // 
            this.dgvTipoCombustible.AllowUserToAddRows = false;
            this.dgvTipoCombustible.AllowUserToDeleteRows = false;
            this.dgvTipoCombustible.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTipoCombustible.Location = new System.Drawing.Point(12, 113);
            this.dgvTipoCombustible.Name = "dgvTipoCombustible";
            this.dgvTipoCombustible.ReadOnly = true;
            this.dgvTipoCombustible.Size = new System.Drawing.Size(301, 203);
            this.dgvTipoCombustible.TabIndex = 32;
            // 
            // btnTipoCombustibleCancelar
            // 
            this.btnTipoCombustibleCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoCombustibleCancelar.Location = new System.Drawing.Point(422, 286);
            this.btnTipoCombustibleCancelar.Name = "btnTipoCombustibleCancelar";
            this.btnTipoCombustibleCancelar.Size = new System.Drawing.Size(90, 30);
            this.btnTipoCombustibleCancelar.TabIndex = 39;
            this.btnTipoCombustibleCancelar.Text = "Cancelar";
            this.btnTipoCombustibleCancelar.UseVisualStyleBackColor = true;
            this.btnTipoCombustibleCancelar.Click += new System.EventHandler(this.btnTipoCombustibleCancelar_Click);
            // 
            // lblTipoCombustibleId
            // 
            this.lblTipoCombustibleId.AutoSize = true;
            this.lblTipoCombustibleId.BackColor = System.Drawing.SystemColors.Control;
            this.lblTipoCombustibleId.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoCombustibleId.Location = new System.Drawing.Point(462, 50);
            this.lblTipoCombustibleId.Name = "lblTipoCombustibleId";
            this.lblTipoCombustibleId.Size = new System.Drawing.Size(121, 29);
            this.lblTipoCombustibleId.TabIndex = 48;
            this.lblTipoCombustibleId.Text = "Comb. ID";
            // 
            // lblCombustibleBuscar
            // 
            this.lblCombustibleBuscar.AutoSize = true;
            this.lblCombustibleBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCombustibleBuscar.Location = new System.Drawing.Point(12, 80);
            this.lblCombustibleBuscar.Name = "lblCombustibleBuscar";
            this.lblCombustibleBuscar.Size = new System.Drawing.Size(63, 20);
            this.lblCombustibleBuscar.TabIndex = 50;
            this.lblCombustibleBuscar.Text = "Buscar:";
            // 
            // txtCombustibleBuscar
            // 
            this.txtCombustibleBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCombustibleBuscar.Location = new System.Drawing.Point(81, 77);
            this.txtCombustibleBuscar.Name = "txtCombustibleBuscar";
            this.txtCombustibleBuscar.Size = new System.Drawing.Size(157, 26);
            this.txtCombustibleBuscar.TabIndex = 49;
            this.txtCombustibleBuscar.TextChanged += new System.EventHandler(this.txtCombustibleBuscar_TextChanged);
            // 
            // TipoCombustibleCrear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 328);
            this.Controls.Add(this.lblCombustibleBuscar);
            this.Controls.Add(this.txtCombustibleBuscar);
            this.Controls.Add(this.lblTipoCombustibleId);
            this.Controls.Add(this.btnTipoCombustibleCancelar);
            this.Controls.Add(this.lblTipoCombustibleDescripcion);
            this.Controls.Add(this.txtTipoCombustibleDescripcion);
            this.Controls.Add(this.btnTipoCombustibleEliminar);
            this.Controls.Add(this.btnTipoCombustibleEditar);
            this.Controls.Add(this.btnTipoCombustibleGuardar);
            this.Controls.Add(this.lblTipoCombustibleTitulo);
            this.Controls.Add(this.dgvTipoCombustible);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TipoCombustibleCrear";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agrgar Tipo de Combustible";
            this.Load += new System.EventHandler(this.TipoCombustible_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTipoCombustible)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTipoCombustibleDescripcion;
        private System.Windows.Forms.TextBox txtTipoCombustibleDescripcion;
        private System.Windows.Forms.Button btnTipoCombustibleEliminar;
        private System.Windows.Forms.Button btnTipoCombustibleEditar;
        private System.Windows.Forms.Button btnTipoCombustibleGuardar;
        private System.Windows.Forms.Label lblTipoCombustibleTitulo;
        private System.Windows.Forms.DataGridView dgvTipoCombustible;
        private System.Windows.Forms.Button btnTipoCombustibleCancelar;
        private System.Windows.Forms.Label lblTipoCombustibleId;
        private System.Windows.Forms.Label lblCombustibleBuscar;
        private System.Windows.Forms.TextBox txtCombustibleBuscar;
    }
}