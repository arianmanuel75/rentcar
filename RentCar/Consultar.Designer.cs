﻿namespace RentCar
{
    partial class Consultar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Consultar));
            this.btnConsultaLCancelar = new System.Windows.Forms.Button();
            this.dgvConsulta = new System.Windows.Forms.DataGridView();
            this.lblTituloConsulta = new System.Windows.Forms.Label();
            this.lblConsultaBuscar = new System.Windows.Forms.Label();
            this.txtConsultaBuscar = new System.Windows.Forms.TextBox();
            this.btnConsultaExportar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsulta)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConsultaLCancelar
            // 
            this.btnConsultaLCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultaLCancelar.Location = new System.Drawing.Point(759, 436);
            this.btnConsultaLCancelar.Name = "btnConsultaLCancelar";
            this.btnConsultaLCancelar.Size = new System.Drawing.Size(90, 30);
            this.btnConsultaLCancelar.TabIndex = 35;
            this.btnConsultaLCancelar.Text = "Cancelar";
            this.btnConsultaLCancelar.UseVisualStyleBackColor = true;
            this.btnConsultaLCancelar.Click += new System.EventHandler(this.btnConsultaLCancelar_Click);
            // 
            // dgvConsulta
            // 
            this.dgvConsulta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsulta.Location = new System.Drawing.Point(12, 78);
            this.dgvConsulta.Name = "dgvConsulta";
            this.dgvConsulta.Size = new System.Drawing.Size(837, 336);
            this.dgvConsulta.TabIndex = 32;
            // 
            // lblTituloConsulta
            // 
            this.lblTituloConsulta.AutoSize = true;
            this.lblTituloConsulta.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloConsulta.Location = new System.Drawing.Point(12, 9);
            this.lblTituloConsulta.Name = "lblTituloConsulta";
            this.lblTituloConsulta.Size = new System.Drawing.Size(153, 38);
            this.lblTituloConsulta.TabIndex = 31;
            this.lblTituloConsulta.Text = "Consulta";
            // 
            // lblConsultaBuscar
            // 
            this.lblConsultaBuscar.AutoSize = true;
            this.lblConsultaBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultaBuscar.Location = new System.Drawing.Point(623, 28);
            this.lblConsultaBuscar.Name = "lblConsultaBuscar";
            this.lblConsultaBuscar.Size = new System.Drawing.Size(63, 20);
            this.lblConsultaBuscar.TabIndex = 37;
            this.lblConsultaBuscar.Text = "Buscar:";
            // 
            // txtConsultaBuscar
            // 
            this.txtConsultaBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConsultaBuscar.Location = new System.Drawing.Point(692, 25);
            this.txtConsultaBuscar.Name = "txtConsultaBuscar";
            this.txtConsultaBuscar.Size = new System.Drawing.Size(157, 26);
            this.txtConsultaBuscar.TabIndex = 36;
            this.txtConsultaBuscar.TextChanged += new System.EventHandler(this.txtConsultaBuscar_TextChanged);
            // 
            // btnConsultaExportar
            // 
            this.btnConsultaExportar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultaExportar.Location = new System.Drawing.Point(663, 436);
            this.btnConsultaExportar.Name = "btnConsultaExportar";
            this.btnConsultaExportar.Size = new System.Drawing.Size(90, 30);
            this.btnConsultaExportar.TabIndex = 38;
            this.btnConsultaExportar.Text = "Exportar";
            this.btnConsultaExportar.UseVisualStyleBackColor = true;
            this.btnConsultaExportar.Click += new System.EventHandler(this.btnConsultaExportar_Click);
            // 
            // Consultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 486);
            this.Controls.Add(this.btnConsultaExportar);
            this.Controls.Add(this.lblConsultaBuscar);
            this.Controls.Add(this.txtConsultaBuscar);
            this.Controls.Add(this.btnConsultaLCancelar);
            this.Controls.Add(this.dgvConsulta);
            this.Controls.Add(this.lblTituloConsulta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Consultar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar";
            this.Load += new System.EventHandler(this.Consultar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsulta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConsultaLCancelar;
        private System.Windows.Forms.DataGridView dgvConsulta;
        private System.Windows.Forms.Label lblTituloConsulta;
        private System.Windows.Forms.Label lblConsultaBuscar;
        private System.Windows.Forms.TextBox txtConsultaBuscar;
        private System.Windows.Forms.Button btnConsultaExportar;
    }
}