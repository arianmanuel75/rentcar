﻿namespace RentCar
{
    partial class VehiculoCrear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehiculoCrear));
            this.lblVehiculoId = new System.Windows.Forms.Label();
            this.lblCliNombre = new System.Windows.Forms.Label();
            this.txtVehiculoDescripcion = new System.Windows.Forms.TextBox();
            this.lblInspecCliente = new System.Windows.Forms.Label();
            this.lblInspecVehiculo = new System.Windows.Forms.Label();
            this.cbVehiculoMarca = new System.Windows.Forms.ComboBox();
            this.cbVehiculoTipo = new System.Windows.Forms.ComboBox();
            this.lblTituloVehiculo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbVehiculoCombustible = new System.Windows.Forms.ComboBox();
            this.cbVehiculoModelo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtVehiculoChasis = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtVehiculoPlaca = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVehiculoMotor = new System.Windows.Forms.TextBox();
            this.lblCliEstado = new System.Windows.Forms.Label();
            this.ckbVehiculoEstado = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnVehiculoCancelar = new System.Windows.Forms.Button();
            this.btnVehiculoGuardar = new System.Windows.Forms.Button();
            this.btnVehiculoTipo = new System.Windows.Forms.Button();
            this.btnVehiculoModelo = new System.Windows.Forms.Button();
            this.btnVehiculoMarca = new System.Windows.Forms.Button();
            this.btnVehiculoComb = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblVehiculoId
            // 
            this.lblVehiculoId.AutoSize = true;
            this.lblVehiculoId.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVehiculoId.Location = new System.Drawing.Point(566, 58);
            this.lblVehiculoId.Name = "lblVehiculoId";
            this.lblVehiculoId.Size = new System.Drawing.Size(146, 29);
            this.lblVehiculoId.TabIndex = 103;
            this.lblVehiculoId.Text = "Vehiculo ID";
            // 
            // lblCliNombre
            // 
            this.lblCliNombre.AutoSize = true;
            this.lblCliNombre.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliNombre.Location = new System.Drawing.Point(22, 124);
            this.lblCliNombre.Name = "lblCliNombre";
            this.lblCliNombre.Size = new System.Drawing.Size(107, 19);
            this.lblCliNombre.TabIndex = 102;
            this.lblCliNombre.Text = "Descripcion:";
            // 
            // txtVehiculoDescripcion
            // 
            this.txtVehiculoDescripcion.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehiculoDescripcion.Location = new System.Drawing.Point(135, 121);
            this.txtVehiculoDescripcion.Name = "txtVehiculoDescripcion";
            this.txtVehiculoDescripcion.Size = new System.Drawing.Size(200, 26);
            this.txtVehiculoDescripcion.TabIndex = 101;
            // 
            // lblInspecCliente
            // 
            this.lblInspecCliente.AutoSize = true;
            this.lblInspecCliente.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecCliente.Location = new System.Drawing.Point(22, 168);
            this.lblInspecCliente.Name = "lblInspecCliente";
            this.lblInspecCliente.Size = new System.Drawing.Size(61, 19);
            this.lblInspecCliente.TabIndex = 100;
            this.lblInspecCliente.Text = "Marca:";
            // 
            // lblInspecVehiculo
            // 
            this.lblInspecVehiculo.AutoSize = true;
            this.lblInspecVehiculo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspecVehiculo.Location = new System.Drawing.Point(365, 124);
            this.lblInspecVehiculo.Name = "lblInspecVehiculo";
            this.lblInspecVehiculo.Size = new System.Drawing.Size(141, 19);
            this.lblInspecVehiculo.TabIndex = 99;
            this.lblInspecVehiculo.Text = "Tipo de vehiculo:";
            // 
            // cbVehiculoMarca
            // 
            this.cbVehiculoMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVehiculoMarca.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVehiculoMarca.FormattingEnabled = true;
            this.cbVehiculoMarca.Location = new System.Drawing.Point(89, 165);
            this.cbVehiculoMarca.Name = "cbVehiculoMarca";
            this.cbVehiculoMarca.Size = new System.Drawing.Size(200, 26);
            this.cbVehiculoMarca.TabIndex = 98;
            // 
            // cbVehiculoTipo
            // 
            this.cbVehiculoTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVehiculoTipo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVehiculoTipo.FormattingEnabled = true;
            this.cbVehiculoTipo.Location = new System.Drawing.Point(512, 121);
            this.cbVehiculoTipo.Name = "cbVehiculoTipo";
            this.cbVehiculoTipo.Size = new System.Drawing.Size(163, 26);
            this.cbVehiculoTipo.TabIndex = 97;
            // 
            // lblTituloVehiculo
            // 
            this.lblTituloVehiculo.AutoSize = true;
            this.lblTituloVehiculo.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloVehiculo.Location = new System.Drawing.Point(12, 9);
            this.lblTituloVehiculo.Name = "lblTituloVehiculo";
            this.lblTituloVehiculo.Size = new System.Drawing.Size(289, 38);
            this.lblTituloVehiculo.TabIndex = 96;
            this.lblTituloVehiculo.Text = "Agregar Vehiculo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 218);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 19);
            this.label1.TabIndex = 107;
            this.label1.Text = "Tipo de combustible:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(399, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 19);
            this.label2.TabIndex = 106;
            this.label2.Text = "Modelo:";
            // 
            // cbVehiculoCombustible
            // 
            this.cbVehiculoCombustible.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVehiculoCombustible.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVehiculoCombustible.FormattingEnabled = true;
            this.cbVehiculoCombustible.Location = new System.Drawing.Point(198, 215);
            this.cbVehiculoCombustible.Name = "cbVehiculoCombustible";
            this.cbVehiculoCombustible.Size = new System.Drawing.Size(161, 26);
            this.cbVehiculoCombustible.TabIndex = 105;
            // 
            // cbVehiculoModelo
            // 
            this.cbVehiculoModelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVehiculoModelo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVehiculoModelo.FormattingEnabled = true;
            this.cbVehiculoModelo.Location = new System.Drawing.Point(476, 165);
            this.cbVehiculoModelo.Name = "cbVehiculoModelo";
            this.cbVehiculoModelo.Size = new System.Drawing.Size(200, 26);
            this.cbVehiculoModelo.TabIndex = 104;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(441, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 19);
            this.label3.TabIndex = 109;
            this.label3.Text = "No. Chasis:";
            // 
            // txtVehiculoChasis
            // 
            this.txtVehiculoChasis.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehiculoChasis.Location = new System.Drawing.Point(545, 215);
            this.txtVehiculoChasis.Name = "txtVehiculoChasis";
            this.txtVehiculoChasis.Size = new System.Drawing.Size(167, 26);
            this.txtVehiculoChasis.TabIndex = 108;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(419, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 19);
            this.label4.TabIndex = 113;
            this.label4.Text = "No. Placa:";
            // 
            // txtVehiculoPlaca
            // 
            this.txtVehiculoPlaca.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehiculoPlaca.Location = new System.Drawing.Point(512, 262);
            this.txtVehiculoPlaca.Name = "txtVehiculoPlaca";
            this.txtVehiculoPlaca.Size = new System.Drawing.Size(200, 26);
            this.txtVehiculoPlaca.TabIndex = 112;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 265);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 19);
            this.label5.TabIndex = 111;
            this.label5.Text = "No. Motor";
            // 
            // txtVehiculoMotor
            // 
            this.txtVehiculoMotor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehiculoMotor.Location = new System.Drawing.Point(111, 262);
            this.txtVehiculoMotor.Name = "txtVehiculoMotor";
            this.txtVehiculoMotor.Size = new System.Drawing.Size(200, 26);
            this.txtVehiculoMotor.TabIndex = 110;
            // 
            // lblCliEstado
            // 
            this.lblCliEstado.AutoSize = true;
            this.lblCliEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliEstado.Location = new System.Drawing.Point(22, 310);
            this.lblCliEstado.Name = "lblCliEstado";
            this.lblCliEstado.Size = new System.Drawing.Size(73, 24);
            this.lblCliEstado.TabIndex = 115;
            this.lblCliEstado.Text = "Estado:";
            // 
            // ckbVehiculoEstado
            // 
            this.ckbVehiculoEstado.AutoSize = true;
            this.ckbVehiculoEstado.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbVehiculoEstado.Location = new System.Drawing.Point(101, 317);
            this.ckbVehiculoEstado.Name = "ckbVehiculoEstado";
            this.ckbVehiculoEstado.Size = new System.Drawing.Size(15, 14);
            this.ckbVehiculoEstado.TabIndex = 114;
            this.ckbVehiculoEstado.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(12, 356);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(700, 2);
            this.label6.TabIndex = 118;
            // 
            // btnVehiculoCancelar
            // 
            this.btnVehiculoCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVehiculoCancelar.Location = new System.Drawing.Point(622, 370);
            this.btnVehiculoCancelar.Name = "btnVehiculoCancelar";
            this.btnVehiculoCancelar.Size = new System.Drawing.Size(90, 30);
            this.btnVehiculoCancelar.TabIndex = 116;
            this.btnVehiculoCancelar.Text = "Cancelar";
            this.btnVehiculoCancelar.UseVisualStyleBackColor = true;
            this.btnVehiculoCancelar.Click += new System.EventHandler(this.btnVehiculoCancelar_Click);
            // 
            // btnVehiculoGuardar
            // 
            this.btnVehiculoGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVehiculoGuardar.Location = new System.Drawing.Point(513, 370);
            this.btnVehiculoGuardar.Name = "btnVehiculoGuardar";
            this.btnVehiculoGuardar.Size = new System.Drawing.Size(90, 30);
            this.btnVehiculoGuardar.TabIndex = 117;
            this.btnVehiculoGuardar.Text = "Guardar";
            this.btnVehiculoGuardar.UseVisualStyleBackColor = true;
            this.btnVehiculoGuardar.Click += new System.EventHandler(this.btnVehiculoGuardar_Click);
            // 
            // btnVehiculoTipo
            // 
            this.btnVehiculoTipo.Image = ((System.Drawing.Image)(resources.GetObject("btnVehiculoTipo.Image")));
            this.btnVehiculoTipo.Location = new System.Drawing.Point(682, 119);
            this.btnVehiculoTipo.Name = "btnVehiculoTipo";
            this.btnVehiculoTipo.Size = new System.Drawing.Size(30, 30);
            this.btnVehiculoTipo.TabIndex = 119;
            this.btnVehiculoTipo.UseVisualStyleBackColor = true;
            this.btnVehiculoTipo.Click += new System.EventHandler(this.btnVehiculoTipo_Click);
            // 
            // btnVehiculoModelo
            // 
            this.btnVehiculoModelo.Image = ((System.Drawing.Image)(resources.GetObject("btnVehiculoModelo.Image")));
            this.btnVehiculoModelo.Location = new System.Drawing.Point(682, 164);
            this.btnVehiculoModelo.Name = "btnVehiculoModelo";
            this.btnVehiculoModelo.Size = new System.Drawing.Size(30, 30);
            this.btnVehiculoModelo.TabIndex = 120;
            this.btnVehiculoModelo.UseVisualStyleBackColor = true;
            this.btnVehiculoModelo.Click += new System.EventHandler(this.btnVehiculoModelo_Click);
            // 
            // btnVehiculoMarca
            // 
            this.btnVehiculoMarca.Image = ((System.Drawing.Image)(resources.GetObject("btnVehiculoMarca.Image")));
            this.btnVehiculoMarca.Location = new System.Drawing.Point(295, 164);
            this.btnVehiculoMarca.Name = "btnVehiculoMarca";
            this.btnVehiculoMarca.Size = new System.Drawing.Size(30, 30);
            this.btnVehiculoMarca.TabIndex = 121;
            this.btnVehiculoMarca.UseVisualStyleBackColor = true;
            this.btnVehiculoMarca.Click += new System.EventHandler(this.btnVehiculoMarca_Click);
            // 
            // btnVehiculoComb
            // 
            this.btnVehiculoComb.Image = ((System.Drawing.Image)(resources.GetObject("btnVehiculoComb.Image")));
            this.btnVehiculoComb.Location = new System.Drawing.Point(365, 214);
            this.btnVehiculoComb.Name = "btnVehiculoComb";
            this.btnVehiculoComb.Size = new System.Drawing.Size(30, 30);
            this.btnVehiculoComb.TabIndex = 122;
            this.btnVehiculoComb.UseVisualStyleBackColor = true;
            this.btnVehiculoComb.Click += new System.EventHandler(this.btnVehiculoComb_Click);
            // 
            // VehiculoCrear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 413);
            this.Controls.Add(this.btnVehiculoComb);
            this.Controls.Add(this.btnVehiculoMarca);
            this.Controls.Add(this.btnVehiculoModelo);
            this.Controls.Add(this.btnVehiculoTipo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnVehiculoCancelar);
            this.Controls.Add(this.btnVehiculoGuardar);
            this.Controls.Add(this.lblCliEstado);
            this.Controls.Add(this.ckbVehiculoEstado);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtVehiculoPlaca);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtVehiculoMotor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtVehiculoChasis);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbVehiculoCombustible);
            this.Controls.Add(this.cbVehiculoModelo);
            this.Controls.Add(this.lblVehiculoId);
            this.Controls.Add(this.lblCliNombre);
            this.Controls.Add(this.txtVehiculoDescripcion);
            this.Controls.Add(this.lblInspecCliente);
            this.Controls.Add(this.lblInspecVehiculo);
            this.Controls.Add(this.cbVehiculoMarca);
            this.Controls.Add(this.cbVehiculoTipo);
            this.Controls.Add(this.lblTituloVehiculo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VehiculoCrear";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Vehiculo";
            this.Load += new System.EventHandler(this.VehiculoCrear_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblVehiculoId;
        private System.Windows.Forms.Label lblCliNombre;
        private System.Windows.Forms.TextBox txtVehiculoDescripcion;
        private System.Windows.Forms.Label lblInspecCliente;
        private System.Windows.Forms.Label lblInspecVehiculo;
        private System.Windows.Forms.ComboBox cbVehiculoMarca;
        private System.Windows.Forms.ComboBox cbVehiculoTipo;
        private System.Windows.Forms.Label lblTituloVehiculo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbVehiculoCombustible;
        private System.Windows.Forms.ComboBox cbVehiculoModelo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtVehiculoChasis;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtVehiculoPlaca;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVehiculoMotor;
        private System.Windows.Forms.Label lblCliEstado;
        private System.Windows.Forms.CheckBox ckbVehiculoEstado;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnVehiculoCancelar;
        private System.Windows.Forms.Button btnVehiculoGuardar;
        private System.Windows.Forms.Button btnVehiculoTipo;
        private System.Windows.Forms.Button btnVehiculoModelo;
        private System.Windows.Forms.Button btnVehiculoMarca;
        private System.Windows.Forms.Button btnVehiculoComb;
    }
}