﻿namespace RentCar
{
    partial class Empleado
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Empleado));
            this.lblTituloEmpleados = new System.Windows.Forms.Label();
            this.lblListaEmpleados = new System.Windows.Forms.Label();
            this.lblAgregarEmpleado = new System.Windows.Forms.Label();
            this.btnClienteAgregar = new System.Windows.Forms.Button();
            this.btnClienteLista = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTituloEmpleados
            // 
            this.lblTituloEmpleados.AutoSize = true;
            this.lblTituloEmpleados.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloEmpleados.Location = new System.Drawing.Point(3, 0);
            this.lblTituloEmpleados.Name = "lblTituloEmpleados";
            this.lblTituloEmpleados.Size = new System.Drawing.Size(189, 38);
            this.lblTituloEmpleados.TabIndex = 4;
            this.lblTituloEmpleados.Text = "Empleados";
            // 
            // lblListaEmpleados
            // 
            this.lblListaEmpleados.AutoSize = true;
            this.lblListaEmpleados.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListaEmpleados.Location = new System.Drawing.Point(463, 122);
            this.lblListaEmpleados.Name = "lblListaEmpleados";
            this.lblListaEmpleados.Size = new System.Drawing.Size(191, 23);
            this.lblListaEmpleados.TabIndex = 12;
            this.lblListaEmpleados.Text = "Lista de empleados";
            // 
            // lblAgregarEmpleado
            // 
            this.lblAgregarEmpleado.AutoSize = true;
            this.lblAgregarEmpleado.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgregarEmpleado.Location = new System.Drawing.Point(119, 122);
            this.lblAgregarEmpleado.Name = "lblAgregarEmpleado";
            this.lblAgregarEmpleado.Size = new System.Drawing.Size(188, 23);
            this.lblAgregarEmpleado.TabIndex = 11;
            this.lblAgregarEmpleado.Text = "Agregar Empleado";
            // 
            // btnClienteAgregar
            // 
            this.btnClienteAgregar.BackColor = System.Drawing.Color.White;
            this.btnClienteAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClienteAgregar.Image = ((System.Drawing.Image)(resources.GetObject("btnClienteAgregar.Image")));
            this.btnClienteAgregar.Location = new System.Drawing.Point(140, 148);
            this.btnClienteAgregar.Name = "btnClienteAgregar";
            this.btnClienteAgregar.Size = new System.Drawing.Size(144, 144);
            this.btnClienteAgregar.TabIndex = 10;
            this.btnClienteAgregar.UseVisualStyleBackColor = false;
            this.btnClienteAgregar.Click += new System.EventHandler(this.btnClienteAgregar_Click);
            // 
            // btnClienteLista
            // 
            this.btnClienteLista.BackColor = System.Drawing.Color.White;
            this.btnClienteLista.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClienteLista.Image = ((System.Drawing.Image)(resources.GetObject("btnClienteLista.Image")));
            this.btnClienteLista.Location = new System.Drawing.Point(488, 148);
            this.btnClienteLista.Name = "btnClienteLista";
            this.btnClienteLista.Size = new System.Drawing.Size(144, 144);
            this.btnClienteLista.TabIndex = 9;
            this.btnClienteLista.UseVisualStyleBackColor = false;
            this.btnClienteLista.Click += new System.EventHandler(this.btnClienteLista_Click);
            // 
            // Empleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblListaEmpleados);
            this.Controls.Add(this.lblAgregarEmpleado);
            this.Controls.Add(this.btnClienteAgregar);
            this.Controls.Add(this.btnClienteLista);
            this.Controls.Add(this.lblTituloEmpleados);
            this.Name = "Empleado";
            this.Size = new System.Drawing.Size(818, 478);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTituloEmpleados;
        private System.Windows.Forms.Label lblListaEmpleados;
        private System.Windows.Forms.Label lblAgregarEmpleado;
        private System.Windows.Forms.Button btnClienteAgregar;
        private System.Windows.Forms.Button btnClienteLista;
    }
}
