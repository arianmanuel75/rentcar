﻿namespace RentCar
{
    partial class Vehiculos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vehiculos));
            this.lblTituloVehiculo = new System.Windows.Forms.Label();
            this.lblListaClientes = new System.Windows.Forms.Label();
            this.lblAgregarCliente = new System.Windows.Forms.Label();
            this.btnClienteAgregar = new System.Windows.Forms.Button();
            this.btnClienteLista = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTituloVehiculo
            // 
            this.lblTituloVehiculo.AutoSize = true;
            this.lblTituloVehiculo.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloVehiculo.Location = new System.Drawing.Point(3, 0);
            this.lblTituloVehiculo.Name = "lblTituloVehiculo";
            this.lblTituloVehiculo.Size = new System.Drawing.Size(167, 38);
            this.lblTituloVehiculo.TabIndex = 2;
            this.lblTituloVehiculo.Text = "Vehiculos";
            // 
            // lblListaClientes
            // 
            this.lblListaClientes.AutoSize = true;
            this.lblListaClientes.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListaClientes.Location = new System.Drawing.Point(474, 122);
            this.lblListaClientes.Name = "lblListaClientes";
            this.lblListaClientes.Size = new System.Drawing.Size(174, 23);
            this.lblListaClientes.TabIndex = 12;
            this.lblListaClientes.Text = "Lista de Vehiculos";
            // 
            // lblAgregarCliente
            // 
            this.lblAgregarCliente.AutoSize = true;
            this.lblAgregarCliente.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgregarCliente.Location = new System.Drawing.Point(127, 122);
            this.lblAgregarCliente.Name = "lblAgregarCliente";
            this.lblAgregarCliente.Size = new System.Drawing.Size(173, 23);
            this.lblAgregarCliente.TabIndex = 11;
            this.lblAgregarCliente.Text = "Agregar Vehiculo";
            // 
            // btnClienteAgregar
            // 
            this.btnClienteAgregar.BackColor = System.Drawing.Color.White;
            this.btnClienteAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClienteAgregar.Image = ((System.Drawing.Image)(resources.GetObject("btnClienteAgregar.Image")));
            this.btnClienteAgregar.Location = new System.Drawing.Point(140, 148);
            this.btnClienteAgregar.Name = "btnClienteAgregar";
            this.btnClienteAgregar.Size = new System.Drawing.Size(144, 144);
            this.btnClienteAgregar.TabIndex = 10;
            this.btnClienteAgregar.UseVisualStyleBackColor = false;
            this.btnClienteAgregar.Click += new System.EventHandler(this.btnClienteAgregar_Click);
            // 
            // btnClienteLista
            // 
            this.btnClienteLista.BackColor = System.Drawing.Color.White;
            this.btnClienteLista.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClienteLista.Image = ((System.Drawing.Image)(resources.GetObject("btnClienteLista.Image")));
            this.btnClienteLista.Location = new System.Drawing.Point(488, 148);
            this.btnClienteLista.Name = "btnClienteLista";
            this.btnClienteLista.Size = new System.Drawing.Size(144, 144);
            this.btnClienteLista.TabIndex = 9;
            this.btnClienteLista.UseVisualStyleBackColor = false;
            this.btnClienteLista.Click += new System.EventHandler(this.btnClienteLista_Click);
            // 
            // Vehiculos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblListaClientes);
            this.Controls.Add(this.lblAgregarCliente);
            this.Controls.Add(this.btnClienteAgregar);
            this.Controls.Add(this.btnClienteLista);
            this.Controls.Add(this.lblTituloVehiculo);
            this.Name = "Vehiculos";
            this.Size = new System.Drawing.Size(818, 478);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblTituloVehiculo;
        private System.Windows.Forms.Label lblListaClientes;
        private System.Windows.Forms.Label lblAgregarCliente;
        private System.Windows.Forms.Button btnClienteAgregar;
        private System.Windows.Forms.Button btnClienteLista;
    }
}
