﻿using RentCar.Service.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar
{
    public partial class Reporte : Form
    {
        private readonly InspeccionService _inspeccionService;
        public Reporte()
        {
            InitializeComponent();
            _inspeccionService = new InspeccionService();
        }

        private void Reporte_Load(object sender, EventArgs e)
        {
            dgvReporte.DataSource = _inspeccionService.GetReporte();
        }

        private void btnReporteLCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void txtReporteBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtReporteBuscar.Text != string.Empty)
            {
                var search = _inspeccionService.GetReporte().Where(x => x.DescripcionVehiculo.Contains(txtReporteBuscar.Text));
                dgvReporte.DataSource = search.ToList();
            }
            else
            {
                dgvReporte.DataSource = _inspeccionService.GetReporte();
            }
        }

        private void btnReporteExportar_Click(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today;

            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel.Worksheet worsheet = null;
            worsheet = workbook.Sheets["Hoja1"];
            worsheet = workbook.ActiveSheet;
            worsheet.Name = "RentCarReporte";

            for (int i = 1; i < dgvReporte.Columns.Count+1; i++)
            {
                worsheet.Cells[1, i] = dgvReporte.Columns[i - 1].HeaderText;
            }

            for (int i = 0; i < dgvReporte.Rows.Count; i++)
            {
                for (int j = 0; j < dgvReporte.Columns.Count; j++)
                {
                    worsheet.Cells[i + 2, j + 1] = dgvReporte.Rows[i].Cells[j].Value.ToString();
                }
            }

            var saveFileDialoge = new SaveFileDialog();
            saveFileDialoge.FileName = "Reporte " + today.ToString("dd-MM-yyyy");
            saveFileDialoge.DefaultExt = ".xlsx";

            if (saveFileDialoge.ShowDialog() == DialogResult.OK)
            {
                workbook.SaveAs(saveFileDialoge.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            app.Quit();
        }
    }
}
